# Stage 1: Build the Java application
FROM maven:3.8.2-jdk-11 AS build

# Copy the code into the container
COPY . .

# Build the application with Maven
RUN mvn clean package -Pprod -DskipTests

# Stage 2: Create the final runtime image
FROM openjdk:11-jdk-slim

# Install Ghostscript for PDF processing
RUN apt-get update && apt-get install -y ghostscript

# Install Tesseract OCR and related libraries
RUN apt-get update && apt-get install -y tesseract-ocr tesseract-ocr-por tesseract-ocr-osd libtesseract-dev

# Set the TESSDATA_PREFIX environment variable to point to the Tesseract data directory
ENV TESSDATA_PREFIX=/usr/share/tesseract-ocr/4.00/tessdata

# Copy the built JAR file from the build stage (adjust the path if needed)
COPY --from=build target/FoodRescueApp-0.0.1-SNAPSHOT.jar FoodRescueApp.jar

# Specify the entry point to run your Java application
ENTRYPOINT ["java", "-jar", "FoodRescueApp.jar"]
